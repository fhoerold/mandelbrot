#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <string>
#include <new>  // std::align_val_t

#include "timer.hpp"

class Camera{
public:
  double x, y;
  unsigned long * imgData;
  unsigned long iterLimit;
  std::string filename;

  Camera() : x(0.0), y(0.0),
	     iterLimit(256),
	     filename("!../mandelbrot.fits"),
	     pWidth(800), pHeight(600),	     
  	     width(4.0), height(3.0),
	     fs(fixed_width) {
    imgData = new (std::align_val_t(32)) unsigned long[pWidth * pHeight] {0};
  }

  ~Camera() { delete[] imgData; }

  void setResolution(long int w, long int h);

  inline long int getpWidth() { return pWidth; }
  inline long int getpHeight() { return pHeight; }
  inline unsigned long getResolution() { return pWidth * pHeight; }
  
  void setWidth(double w);
  void setHeight(double h);

  inline double getWidth() { return width; }
  inline double getHeight() { return height; }
  
private:
  long int pWidth, pHeight;  
  double width, height;
  enum fixed_scale {fixed_width, fixed_height};
  fixed_scale fs;
};

#endif /* CAMERA_HPP */
