#include "timer.hpp"

double Timer::time() const {
  if(running) {
    return static_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - t_start).count();
  } else {
    return static_cast<std::chrono::duration<double>>(t_end - t_start).count();
  }
}

unsigned long Timer::hours() const {
  return static_cast<unsigned long>(time() / 3600.0);
}
unsigned long Timer::minutes() const {
  return static_cast<unsigned long>(time() / 60.0);
}
unsigned long Timer::seconds() const {
  return static_cast<unsigned long>(time());
}
unsigned long Timer::milliseconds() const {
  return static_cast<unsigned long>(time() * 1000.0);
}
