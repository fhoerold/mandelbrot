#ifndef MANDELBROT_HPP
#define MANDELBROT_HPP

void initSettings(int argc, char * argv[], struct Camera& cam);
template <typename T>
void mandelBrot(struct Camera& cam);
int writeFITSImage(struct Camera& cam);
void printFITSError(int status);
void printOptionError(std::string option, std::string value);

#endif /* MANDELBROT_HPP */
