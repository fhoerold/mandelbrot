#!/bin/bash

len=1024
x1=0.3
y1=0.03
x2=0.3
y2=-0.03

prog=$1
dir=$2

xlist=""
ylist=""
flist=""
for i in $(seq 0 $((len-1)))
do
    xlist="$xlist $(echo "$x1 + ($x2 - $x1) * $i / $len" | bc -l)"
    ylist="$ylist $(echo "$y1 + ($y2 - $y1) * $i / $len" | bc -l)"
    flist="$flist ${dir}stack_${i}.fits"
done

parallel --keep-order --link --progress \
         $prog -d ${len}x${len} -w 4.0x4.0 -p {1}:{2} -i 18 -f '!{3}' '&>/dev/null' ';' \
         gzip -f3 {3} \
         ::: $xlist ::: $ylist ::: $flist
echo -e "done"
