#include "camera.hpp"

void Camera::setResolution(long int w, long int h) {
  pWidth = w;
  pHeight = h;

  if(fs == fixed_width) {
    setWidth(width);
  } else {
    setHeight(height);
  }
  
  delete[] imgData;
  imgData = new (std::align_val_t(32)) unsigned long[pWidth * pHeight] {0};
}

void Camera::setWidth(double w) {
    width = w;
    height = width * static_cast<double>(pHeight) / static_cast<double>(pWidth);
    fs = fixed_width;
}

void Camera::setHeight(double h) {
    height = h;
    width = height * static_cast<double>(pWidth) / static_cast<double>(pHeight);
    fs = fixed_height;
}
