import random
import math

phi = random.random() * 2.0 * math.pi
r = random.random() * 0.0001

x = math.cos(phi) * (0.25 + r) - 1.0
y = math.sin(phi) * (0.25 + r)
print(str(x) + ":" + str(y))
