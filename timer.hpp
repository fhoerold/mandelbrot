#ifndef TIMER_HPP
#define TIMER_HPP

#include <chrono>  // std::chrono::time_point, std::chrono::high_resolution_clock::now, std::chrono::duration

class Timer {
public:
  Timer() = default;

  inline void start() { running = false; t_start = std::chrono::high_resolution_clock::now(); }
  inline void stop() {  t_end = std::chrono::high_resolution_clock::now(); running = false; }

  double time() const;
  unsigned long hours() const; 
  unsigned long minutes() const;
  unsigned long seconds() const;
  unsigned long milliseconds() const;

private:
  std::chrono::time_point<std::chrono::high_resolution_clock> t_start, t_end;
  bool running;
};

#endif /* TIMER_HPP */
