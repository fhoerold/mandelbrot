ds9 &
pid=$!

# rainbow?
colormaps=(a b bb he sls hsv heat cool viridis)

size=${#colormaps[@]}

while(true);
do
coords=$(python ../findcoords.py)
./mandelbrot -d 4096x2160 -w 0.000001 -p $coords -i 16384 -f !../slide.fits
index=$(($RANDOM % $size))
ds9 ../slide.fits -geometry 4096x2160 -view info no -view panner no -view magnifier no -view buttons no -view colorbar no -zoom 1 -cmap ${colormaps[$index]} -scale log &
sleep 20
kill -KILL $pid
pid=$!
done
