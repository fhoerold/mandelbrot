# Mandelbrot
An optimized Mandelbrot renderer, written in C++

## Building
Make sure CFITSIO is installed. Then:
```
mkdir build
cd build
cmake ..
make
```

## Slideshow
`slideshow.sh` will generate a slideshow of hires Mandelbrot zooms in real time. The images are displayed using ds9 FITS viewer, so that software should be installed.

## Makestack
`makestack.sh` demonstrates a simple bash script to generate a stack of julia sets, for example for 3D printing. It depends on GNU parallel.

