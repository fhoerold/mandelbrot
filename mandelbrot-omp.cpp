#include <iostream>  // std::cout, std::cerr
#include <iomanip>   // std::fixed, std::setprecision, std::setfill, std::setw
#include <string>    // std::compare, std::stoul, std::stod
#include <utility>   // std::pair, std::make_pair
#include <vector>    // std::vector, std::push_back

#include <omp.h> // OpenMP

#include <immintrin.h>  // _mm_fmadd_pd
#include <emmintrin.h>  // _mm_setzero_pd, _mm_setzero_si128, _mm_set_pd, _mm_set_pd1, _mm_add_pd, _mm_sub_pd, _mm_mul_pd, _mm_cmple_pd, _mm_cast_pd_si128
#include <smmintrin.h>  // _mm_test_all_zeros

#include "fitsio.h"  // CFITSIO

#include "mandelbrot.hpp"
#include "camera.hpp"

using arg_t = std::pair<std::string, std::string>;

int main(int argc, char * argv[]){  
  Camera cam;
  Timer timer;
  
  initSettings(argc, argv, cam);

  // Print option details
  std::cout << "\n*****  Parameters  *****\n";
  std::cout << "Dimensions: " << cam.getpWidth() << "x" << cam.getpHeight()
	    << "(" << std::fixed << std::setprecision(2)
	    << cam.getResolution() / 1e6 << "MP)\n";
  std::cout << "Width:      " << cam.getWidth() << "\n";
  std::cout << "Height:     " << cam.getHeight() << "\n";
  std::cout << "X:          " << cam.x << "\n";
  std::cout << "Y:          " << cam.y << "\n";
  std::cout << "Iterations: " << cam.iterLimit << "\n";
  std::cout << "Filename:   " << cam.filename << "\n";
  std::cout << "************************\n" << std::endl;

  // Render Mandelbrot
  std::cout << "Rendering..." << std::flush;

  timer.start();  
  mandelBrot<double>(cam);
  timer.stop();

  std::cout << "\rDone. (" << std::setfill('0')
	    << timer.hours() << ":"
	    << std::setw(2) << timer.minutes() % 60 << ":"
	    << std::setw(2) << timer.seconds() % 60 << ":"
	    << std::setw(3) << timer.milliseconds() % 1000 << ")" << std::endl;
  
  // Write the FITS image
  writeFITSImage(cam);
  
  return 0;
}

void initSettings(int argc, char *argv[], struct Camera& cam){  
  // Variables for input processing
  std::string key, value;
  std::size_t pos;
  std::vector<arg_t> args;

  if(argc % 2 == 0) {
    std::cerr << "One or more option values are missing!" << std::endl;
    exit(1);
  }
  
  for(int i = 1; i < argc; i+=2) {
    args.push_back(std::make_pair(argv[i], argv[i+1]));
  }

  // Brief explanation of command line options
  //
  // --dimensions -d
  // Sets the size of  the output image in pixels
  // Usage: -d [width int]x[height int]
  //
  // --width -w
  // Sets the width of the render area in fractal space
  // Usage: -w [width double];
  //
  // --position -p
  // Sets the center of the render area in fractal space
  // Usage: -p [x position double]:[y position double]
  //
  // --iterations -i
  // Sets the maximimum number of iterations
  // Usage: -i [number of iterations int]
  // --filename -f
  // Sets the filename for the FITS file
  // This is passed firectly to CFITSIO, so the standarad
  // CFITSIO escapecodes may be used. One useful example
  // is the "!" at the beginning of a filename which
  // causes existing files to be overwritten.
  // Usage -f [filename string]

  try {
    for(arg_t arg : args) {
      std::tie(key, value) = arg;
      if(!key.compare("--dimensions") || !key.compare("-d")) {
	long int w = std::stol(value, &pos);
	long int h = std::stol(value.substr(pos+1));
	cam.setResolution(w, h);
	if(!cam.getpWidth() || !cam.getpHeight())
	  printOptionError("dimensions", value);
      } else if (!key.compare("--width") || !key.compare("-w")) {
	cam.setWidth(std::stod(value));
	if(!cam.getWidth())
	  printOptionError("width", value);
      } else if (!key.compare("--position") || !key.compare("-p")) {
	cam.x = std::stod(value, &pos);
	cam.y = std::stod(value.substr(pos+1));
      } else if (!key.compare("--iterations") || !key.compare("-i")) {
	cam.iterLimit = std::stoul(value);
	if(!cam.iterLimit)
	  printOptionError("iterations", value);
      } else if (!key.compare("--filename") || !key.compare("-f")) {
	cam.filename = value;
      } else {
	std::cerr << "Unknown option " << key << std::endl;
	exit(1);
      }
    }
  }
  catch(std::invalid_argument& e) {
    std::cerr << "Incorrect value " << value << " for option " << key << "\n";
  }
  catch(std::out_of_range& e) {
    std::cerr << "Value " << value << " for option " << key << " is out of range.\n";
  }
  catch(...) {
    std::cerr << "A fatal error occured." << std::endl;
    exit(1);
  }
}

template <typename T>
void mandelBrot(struct Camera& cam){
  long int pw = cam.getpWidth();
  long int ph = cam.getpHeight();
  double w = cam.getWidth();
  double h = cam.getHeight();

#pragma omp parallel for
  for(long int y = 0; y < ph; ++y){

    /*
    // 10:285
    for(long int x = 0; x < pw; ++x){
      T cr = (static_cast<T>(x) / static_cast<T>(pw) - 0.5) * w + cam.x;
      T ci = (static_cast<T>(y) / static_cast<T>(ph) - 0.5) * h + cam.y;
      T zr = 0;
      T zi = 0;
      T zr2, zi2;

      unsigned long i;
      for(i = 0; i < cam.iterLimit; ++i){
	zr2 = zr * zr;
	zi2 = zi * zi;
	
	if(zr2 + zi2 > 4.0) {
	  break;
	}

	zi = zr * zi;	
	zr = zr2 - zi2 + cr;	
	zi = zi + zi + ci;
      }

      cam.imgData[y * pw + x] = (i == cam.iterLimit ? 0 : i);
    }
    */

    /*
    // 10:690
    for(long int x = 0; x < pw; ++x){
      T x_cr = (static_cast<T>(x) / static_cast<T>(pw) - 0.5) * w + cam.x;
      T x_ci = (static_cast<T>(y) / static_cast<T>(ph) - 0.5) * h + cam.y;
      T x_zr = 0;
      T x_zi = 0;
      T x_zr2, x_zi2;

      T y_cr = (static_cast<T>(x+1) / static_cast<T>(pw) - 0.5) * w + cam.x;
      T y_ci = (static_cast<T>(y) / static_cast<T>(ph) - 0.5) * h + cam.y;
      T y_zr = 0;
      T y_zi = 0;
      T y_zr2, y_zi2;

      unsigned long x_i = 0, y_i = 0, i;
      for(i = 0; i < cam.iterLimit; ++i){
	x_zr2 = x_zr * x_zr;
	y_zr2 = y_zr * y_zr;
	x_zi2 = x_zi * x_zi;
	y_zi2 = y_zi * y_zi;

	if(x_zr2 + x_zi2 <= 4.0) ++x_i;
	if(y_zr2 + y_zi2 <= 4.0) ++y_i;
	
	if(x_i != i+1 && y_i != i+1) {
	  break;
	}

	x_zi = x_zr * x_zi;
	y_zi = y_zr * y_zi;
	x_zr = x_zr2 - x_zi2 + x_cr;
	y_zr = y_zr2 - y_zi2 + y_cr;
	x_zi = x_zi + x_zi + x_ci;
	y_zi = y_zi + y_zi + y_ci;
      }
      cam.imgData[y * pw + x] = (x_i == cam.iterLimit ? 0 : x_i);
      cam.imgData[y * pw + x+1] = (y_i == cam.iterLimit ? 0 : y_i);
    }
    */    

    /*
    // Only works with doubles for now!
    for(long int x = 0; x < pw; x+=2) {
      __m128d cr = _mm_set_pd((static_cast<T>(x+1) / static_cast<T>(pw) - 0.5) * w + cam.x,
			      (static_cast<T>(x) / static_cast<T>(pw) - 0.5) * w + cam.x);
      __m128d ci = _mm_set_pd1((static_cast<T>(y) / static_cast<T>(ph) - 0.5) * h + cam.y);

      __m128d zr = _mm_setzero_pd(); // xor
      __m128d zi = _mm_setzero_pd();
      __m128d zr2, zi2;

      const __m128d bound = _mm_set_pd1(4.0);
      __m128i cmp;

      __m128i iter = _mm_setzero_si128();

      // __m128d double_zr;
      // __m128d sub;
      
      unsigned long i;
      for(i = 0; i < cam.iterLimit; ++i) {
	zr2 = _mm_mul_pd(zr, zr);
	zi2 = _mm_mul_pd(zi, zi);

	cmp = _mm_castpd_si128(_mm_cmple_pd(_mm_add_pd(zr2, zi2), bound));
	iter = _mm_sub_epi64(iter, cmp);
	if(_mm_test_all_zeros(cmp, cmp)) {
	  break;
	}

	// 5:607
	// zi = _mm_mul_pd(zr, zi);
	// zr = _mm_add_pd(_mm_sub_pd(zr2, zi2), cr);
	// zi = _mm_add_pd(_mm_add_pd(zi, zi), ci);

	// 5:288
	// zi = _mm_fmadd_pd(_mm_add_pd(zr, zr), zi, ci);
	// zr = _mm_add_pd(_mm_sub_pd(zr2, zi2), cr);

	// 5:294
	// double_zr = _mm_add_pd(zr, zr);
	// sub = _mm_sub_pd(zr2, zi2);
	// zi = _mm_fmadd_pd(double_zr, zi, ci);	
	// zr = _mm_add_pd(sub, cr);	  
      }
      _mm_store_si128(reinterpret_cast<__m128i*>(&cam.imgData[y * pw + x]), iter);
      if(cam.imgData[y * pw + x] == cam.iterLimit) cam.imgData[y * pw + x] = 0;
      if(cam.imgData[y * pw + x+1] == cam.iterLimit) cam.imgData[y * pw + x+1] = 0;
    }
    */

    /*
    // Only works with doubles for now!
    for(long int x = 0; x < pw; x+=4) {
      //__m256d cr = _mm256_set_pd((static_cast<T>(x+3) / static_cast<T>(pw) - 0.5) * w + cam.x,
      //			 (static_cast<T>(x+2) / static_cast<T>(pw) - 0.5) * w + cam.x,
      //			 (static_cast<T>(x+1) / static_cast<T>(pw) - 0.5) * w + cam.x,
      //			 (static_cast<T>(x) / static_cast<T>(pw) - 0.5) * w + cam.x);
      
      __m256d cr = _mm256_set_pd(static_cast<T>(x+3), static_cast<T>(x+2),
				 static_cast<T>(x+1), static_cast<T>(x));
      cr = _mm256_div_pd(cr, _mm256_set1_pd(static_cast<T>(pw)));
      cr = _mm256_sub_pd(cr, _mm256_set1_pd(0.5));
      cr = _mm256_fmadd_pd(cr, _mm256_set1_pd(w), _mm256_set1_pd(cam.x)); 

      __m256d ci = _mm256_set1_pd((static_cast<T>(y) / static_cast<T>(ph) - 0.5) * h + cam.y);

      __m256d zr = _mm256_setzero_pd(); // xor
      __m256d zi = _mm256_setzero_pd();
      __m256d zr2, zi2;

      const __m256d bound = _mm256_set1_pd(4.0);
      const __m256i iterLimit = _mm256_set1_epi64x(cam.iterLimit);
      __m256i cmp;

      __m256i iter = _mm256_setzero_si256();

      // __m128d double_zr;
      // __m128d sub;
      
      unsigned long i;
      for(i = 0; i < cam.iterLimit; ++i) {
	zr2 = _mm256_mul_pd(zr, zr);
	zi2 = _mm256_mul_pd(zi, zi);

	cmp = _mm256_castpd_si256(_mm256_cmp_pd(_mm256_add_pd(zr2, zi2), bound, _CMP_LE_OS));
	iter = _mm256_sub_epi64(iter, cmp);
	if(_mm256_testz_si256(cmp, cmp)) {
	  break;
	}

	//
	// zi = _mm_mul_pd(zr, zi);
	// zr = _mm_add_pd(_mm_sub_pd(zr2, zi2), cr);
	// zi = _mm_add_pd(_mm_add_pd(zi, zi), ci);

	// 2:914
	zi = _mm256_fmadd_pd(_mm256_add_pd(zr, zr), zi, ci);
	zr = _mm256_add_pd(_mm256_sub_pd(zr2, zi2), cr);

	//
	// double_zr = _mm_add_pd(zr, zr);
	// sub = _mm_sub_pd(zr2, zi2);
	// zi = _mm_fmadd_pd(double_zr, zi, ci);	
	// zr = _mm_add_pd(sub, cr);	  
      }
      iter = _mm256_andnot_si256(_mm256_cmpeq_epi64(iter, iterLimit), iter);
      _mm256_store_si256(reinterpret_cast<__m256i*>(&cam.imgData[y * pw + x]), iter);
    }
    */

    /*
    // Only works with doubles for now!
    for(long int x = 0; x < pw; x+=8) {
      __m256d x_cr = _mm256_set_pd(static_cast<T>(x+3), static_cast<T>(x+2),
				 static_cast<T>(x+1), static_cast<T>(x));
      __m256d y_cr = _mm256_set_pd(static_cast<T>(x+7), static_cast<T>(x+6),
				 static_cast<T>(x+5), static_cast<T>(x+4));      
      x_cr = _mm256_div_pd(x_cr, _mm256_set1_pd(static_cast<T>(pw)));
      y_cr = _mm256_div_pd(y_cr, _mm256_set1_pd(static_cast<T>(pw)));      
      x_cr = _mm256_sub_pd(x_cr, _mm256_set1_pd(0.5));
      y_cr = _mm256_sub_pd(y_cr, _mm256_set1_pd(0.5));      
      x_cr = _mm256_fmadd_pd(x_cr, _mm256_set1_pd(w), _mm256_set1_pd(cam.x)); 
      y_cr = _mm256_fmadd_pd(y_cr, _mm256_set1_pd(w), _mm256_set1_pd(cam.x)); 
      
      __m256d x_ci = _mm256_set1_pd((static_cast<T>(y) / static_cast<T>(ph) - 0.5) * h + cam.y);
      __m256d y_ci = _mm256_set1_pd((static_cast<T>(y) / static_cast<T>(ph) - 0.5) * h + cam.y);      

      __m256d x_zr = _mm256_setzero_pd();
      __m256d y_zr = _mm256_setzero_pd();    
      __m256d x_zi = _mm256_setzero_pd();
      __m256d y_zi = _mm256_setzero_pd();      
      __m256d x_zr2, x_zi2;
      __m256d y_zr2, y_zi2;      

      const __m256d bound = _mm256_set1_pd(4.0);
      const __m256i iterLimit = _mm256_set1_epi64x(cam.iterLimit);
      __m256i x_cmp;
      __m256i y_cmp;

      __m256i x_iter = _mm256_setzero_si256();
      __m256i y_iter = _mm256_setzero_si256();      

      // __m256d x_double_zr;
      // __m256d y_double_zr;
      // __m256d x_sub;
      // __m256d y_sub;
      
      unsigned long i;
      for(i = 0; i < cam.iterLimit; ++i) {
	x_zr2 = _mm256_mul_pd(x_zr, x_zr);
	y_zr2 = _mm256_mul_pd(y_zr, y_zr);	
	x_zi2 = _mm256_mul_pd(x_zi, x_zi);
	y_zi2 = _mm256_mul_pd(y_zi, y_zi);

	x_cmp = _mm256_castpd_si256(_mm256_cmp_pd(_mm256_add_pd(x_zr2, x_zi2), bound, _CMP_LE_OS));
	y_cmp = _mm256_castpd_si256(_mm256_cmp_pd(_mm256_add_pd(y_zr2, y_zi2), bound, _CMP_LE_OS));	
	x_iter = _mm256_sub_epi64(x_iter, x_cmp);
	y_iter = _mm256_sub_epi64(y_iter, y_cmp);

	if(_mm256_testz_si256(x_cmp, x_cmp) && _mm256_testz_si256(y_cmp, y_cmp)) {
	  break;
	}

	// 1:689
	// x_zi = _mm256_mul_pd(x_zr, x_zi);
	// y_zi = _mm256_mul_pd(y_zr, y_zi);	
	// x_zr = _mm256_add_pd(_mm256_sub_pd(x_zr2, x_zi2), x_cr);
	// y_zr = _mm256_add_pd(_mm256_sub_pd(y_zr2, y_zi2), y_cr);	
	// x_zi = _mm256_add_pd(_mm256_add_pd(x_zi, x_zi), x_ci);
	// y_zi = _mm256_add_pd(_mm256_add_pd(y_zi, y_zi), y_ci);	

	// 1:525
	x_zi = _mm256_fmadd_pd(_mm256_add_pd(x_zr, x_zr), x_zi, x_ci);
	y_zi = _mm256_fmadd_pd(_mm256_add_pd(y_zr, y_zr), y_zi, y_ci);	
	x_zr = _mm256_add_pd(_mm256_sub_pd(x_zr2, x_zi2), x_cr);
	y_zr = _mm256_add_pd(_mm256_sub_pd(y_zr2, y_zi2), y_cr);	

	// 1:521
	// x_double_zr = _mm256_add_pd(x_zr, x_zr);
	// y_double_zr = _mm256_add_pd(y_zr, y_zr);	
	// x_sub = _mm256_sub_pd(x_zr2, x_zi2);
	// y_sub = _mm256_sub_pd(y_zr2, y_zi2);
	// x_zi = _mm256_fmadd_pd(x_double_zr, x_zi, x_ci);	
	// y_zi = _mm256_fmadd_pd(y_double_zr, y_zi, y_ci);	
	// x_zr = _mm256_add_pd(x_sub, x_cr);	  
	// y_zr = _mm256_add_pd(y_sub, y_cr);	  
      }
      x_iter = _mm256_andnot_si256(_mm256_cmpeq_epi64(x_iter, iterLimit), x_iter);
      y_iter = _mm256_andnot_si256(_mm256_cmpeq_epi64(y_iter, iterLimit), y_iter);      
      _mm256_store_si256(reinterpret_cast<__m256i*>(&cam.imgData[y * pw + x]), x_iter);
      _mm256_store_si256(reinterpret_cast<__m256i*>(&cam.imgData[y * pw + x+4]), y_iter);
    }
    */

    // 1:109
    // Only works with doubles for now!
    const int p = 4;    
    for(long int x = 0; x < pw; x+=p*4) {
      
      __m256d cr[p];
      for(int i = 0; i < p; ++i) cr[i] = _mm256_set_pd(static_cast<T>(x+3 + 4*i),
						       static_cast<T>(x+2 + 4*i),
						       static_cast<T>(x+1 + 4*i),
						       static_cast<T>(x + 4*i));
      for(int i = 0; i < p; ++i) cr[i] = _mm256_div_pd(cr[i], _mm256_set1_pd(static_cast<T>(pw)));
      for(int i = 0; i < p; ++i) cr[i] = _mm256_sub_pd(cr[i], _mm256_set1_pd(0.5));
      for(int i = 0; i < p; ++i) cr[i] = _mm256_fmadd_pd(cr[i], _mm256_set1_pd(w), _mm256_set1_pd(cam.x));

      __m256d ci[p];
      for(int i = 0; i < p; ++i)
	ci[i] = _mm256_set1_pd((static_cast<T>(y) / static_cast<T>(ph) - 0.5) * h + cam.y);

      __m256d zr[p];
      for(int i = 0; i < p; ++i) zr[i] = _mm256_setzero_pd();
      __m256d zi[p];
      for(int i = 0; i < p; ++i) zi[i] = _mm256_setzero_pd();
      __m256d zr2[p], zi2[p];

      const __m256d bound = _mm256_set1_pd(4.0);
      const __m256i iterLimit = _mm256_set1_epi64x(cam.iterLimit);
      __m256i cmp[p];

      __m256i iter[p];
      for(int i = 0; i < p; ++i) iter[i] = _mm256_setzero_si256();
      
      unsigned long i;
      for(i = 0; i < cam.iterLimit; ++i) {
	for(int i = 0; i < p; ++i) zr2[i] = _mm256_mul_pd(zr[i], zr[i]);
	for(int i = 0; i < p; ++i) zi2[i] = _mm256_mul_pd(zi[i], zi[i]);

	for(int i = 0; i < p; ++i)
	  cmp[i] = _mm256_castpd_si256(_mm256_cmp_pd(_mm256_add_pd(zr2[i], zi2[i]), bound, _CMP_LE_OS));
	for(int i = 0; i < p; ++i) iter[i] = _mm256_sub_epi64(iter[i], cmp[i]);

	if(_mm256_testz_si256(cmp[0], cmp[0]) && _mm256_testz_si256(cmp[1], cmp[1]) &&
	   _mm256_testz_si256(cmp[2], cmp[2]) && _mm256_testz_si256(cmp[3], cmp[3])) {
	  break;
	}
	
	for(int i = 0; i < p; ++i) zi[i] = _mm256_fmadd_pd(_mm256_add_pd(zr[i], zr[i]), zi[i], ci[i]);
	for(int i = 0; i < p; ++i) zr[i] = _mm256_add_pd(_mm256_sub_pd(zr2[i], zi2[i]), cr[i]);
      }
      for(int i = 0; i < p; ++i)
	iter[i] = _mm256_andnot_si256(_mm256_cmpeq_epi64(iter[i], iterLimit), iter[i]);
      for(int i = 0; i < p; ++i)      
	_mm256_store_si256(reinterpret_cast<__m256i*>(&cam.imgData[y * pw + x + 4*i]), iter[i]);
    }
    
    // times are for:
    // ./mandelbrot -d 4000x3000 -i 2048
  }
}
  
int writeFITSImage(struct Camera& cam){
  fitsfile *fptr;
  int status = 0;
  long fpixel, nelements;
  
  float fversion = 0;
  fits_get_version(&fversion);

  int bitpix = ULONG_IMG;
  long naxis = 2;
  long naxes[2] = {cam.getpWidth(), cam.getpHeight()};

  if(fits_create_file(&fptr, cam.filename.c_str(), &status))
    printFITSError(status);

  if(fits_create_img(fptr, bitpix, naxis, naxes, &status))
    printFITSError(status);

  fpixel = 1;
  nelements = naxes[0] * naxes[1];
  if(fits_write_img(fptr, TULONG, fpixel, nelements, cam.imgData, &status))
    printFITSError(status);

  // Write date and other information to FITS header
  if(fits_write_date(fptr, &status))
    printFITSError(status);

  if(fits_close_file(fptr, &status))
    printFITSError(status);

  std::cout << "Wrote file: " << cam.filename << std::endl;

  return 0;
}

void printFITSError(int status){
  if(status){
    fits_report_error(stderr, status);
    exit(status);
  }
  return;
}

void printOptionError(std::string option, std::string value){
  std::cerr<< "Invalid value " << value << " for " << option << std::endl;
  exit(1);
}
