#include <iostream>  // std::cout, std::cerr
#include <iomanip>   // std::fixed, std::setprecision, std::setfill, std::setw
#include <string>    // std::compare, std::stoul, std::stod
#include <utility>   // std::pair, std::make_pair
#include <vector>    // std::vector, std::push_back

#include <immintrin.h>  // _mm_fmadd_pd
#include <emmintrin.h>  // _mm_setzero_pd, _mm_setzero_si128, _mm_set_pd, _mm_set_pd1, _mm_add_pd, _mm_sub_pd, _mm_mul_pd, _mm_cmple_pd, _mm_cast_pd_si128
#include <smmintrin.h>  // _mm_test_all_zeros

#include "fitsio.h"  // CFITSIO

#include "mandelbrot.hpp"
#include "camera.hpp"

using arg_t = std::pair<std::string, std::string>;

int main(int argc, char * argv[]){  
  Camera cam;
  Timer timer;
  
  initSettings(argc, argv, cam);

  // Print option details
  std::cout << "\n*****  Parameters  *****\n";
  std::cout << "Dimensions: " << cam.getpWidth() << "x" << cam.getpHeight()
	    << "(" << std::fixed << std::setprecision(2)
	    << cam.getResolution() / 1e6 << "MP)\n";
  std::cout << "Width:      " << cam.getWidth() << "\n";
  std::cout << "Height:     " << cam.getHeight() << "\n";
  std::cout << "X:          " << cam.x << "\n";
  std::cout << "Y:          " << cam.y << "\n";
  std::cout << "Iterations: " << cam.iterLimit << "\n";
  std::cout << "Filename:   " << cam.filename << "\n";
  std::cout << "************************\n" << std::endl;

  // Render Mandelbrot
  std::cout << "Rendering..." << std::flush;

  timer.start();  
  mandelBrot<double>(cam);
  timer.stop();

  std::cout << "\rDone. (" << std::setfill('0')
	    << timer.hours() << ":"
	    << std::setw(2) << timer.minutes() % 60 << ":"
	    << std::setw(2) << timer.seconds() % 60 << ":"
	    << std::setw(3) << timer.milliseconds() % 1000 << ")" << std::endl;
  
  // Write the FITS image
  writeFITSImage(cam);
  
  return 0;
}

void initSettings(int argc, char *argv[], struct Camera& cam){  
  // Variables for input processing
  std::string key, value;
  std::size_t pos;
  std::vector<arg_t> args;

  if(argc % 2 == 0) {
    std::cerr << "One or more option values are missing!" << std::endl;
    exit(1);
  }
  
  for(int i = 1; i < argc; i+=2) {
    args.push_back(std::make_pair(argv[i], argv[i+1]));
  }

  // Brief explanation of command line options
  //
  // --dimensions -d
  // Sets the size of  the output image in pixels
  // Usage: -d [width int]x[height int]
  //
  // --width -w
  // Sets the width of the render area in fractal space
  // Usage: -w [width double];
  //
  // --position -p
  // Sets the center of the render area in fractal space
  // Usage: -p [x position double]:[y position double]
  //
  // --iterations -i
  // Sets the maximimum number of iterations
  // Usage: -i [number of iterations int]
  // --filename -f
  // Sets the filename for the FITS file
  // This is passed firectly to CFITSIO, so the standarad
  // CFITSIO escapecodes may be used. One useful example
  // is the "!" at the beginning of a filename which
  // causes existing files to be overwritten.
  // Usage -f [filename string]

  try {
    for(arg_t arg : args) {
      std::tie(key, value) = arg;
      if(!key.compare("--dimensions") || !key.compare("-d")) {
	long int w = std::stol(value, &pos);
	long int h = std::stol(value.substr(pos+1));
	cam.setResolution(w, h);
	if(!cam.getpWidth() || !cam.getpHeight())
	  printOptionError("dimensions", value);
      } else if (!key.compare("--width") || !key.compare("-w")) {
	cam.setWidth(std::stod(value));
	if(!cam.getWidth())
	  printOptionError("width", value);
      } else if (!key.compare("--position") || !key.compare("-p")) {
	cam.x = std::stod(value, &pos);
	cam.y = std::stod(value.substr(pos+1));
      } else if (!key.compare("--iterations") || !key.compare("-i")) {
	cam.iterLimit = std::stoul(value);
	if(!cam.iterLimit)
	  printOptionError("iterations", value);
      } else if (!key.compare("--filename") || !key.compare("-f")) {
	cam.filename = value;
      } else {
	std::cerr << "Unknown option " << key << std::endl;
	exit(1);
      }
    }
  }
  catch(std::invalid_argument& e) {
    std::cerr << "Incorrect value " << value << " for option " << key << "\n";
  }
  catch(std::out_of_range& e) {
    std::cerr << "Value " << value << " for option " << key << " is out of range.\n";
  }
  catch(...) {
    std::cerr << "A fatal error occured." << std::endl;
    exit(1);
  }
}

template <typename T>
void mandelBrot(struct Camera& cam){
  long int pw = cam.getpWidth();
  long int ph = cam.getpHeight();
  double w = cam.getWidth();
  double h = cam.getHeight();
  for(long int y = 0; y < ph; ++y){
    for(long int x = 0; x < pw; ++x){
      T zr = (static_cast<T>(x) / static_cast<T>(pw) - 0.5) * w;
      T zi = (static_cast<T>(y) / static_cast<T>(ph) - 0.5) * h;
      T cr = cam.x;
      T ci = cam.y;
      T zr2, zi2;

      unsigned long i;
      for(i = 0; i < cam.iterLimit; ++i){
	zr2 = zr * zr;
	zi2 = zi * zi;
	
	if(zr2 + zi2 > 4.0) {
	  break;
	}

	zi = zr * zi;	
	zr = zr2 - zi2 + cr;	
	zi = zi + zi + ci;
      }

      cam.imgData[y * pw + x] = (i == cam.iterLimit ? 0 : 1); //(i == cam.iterLimit ? 0 : i);
    }
  }
}
  
int writeFITSImage(struct Camera& cam){
  fitsfile *fptr;
  int status = 0;
  long fpixel, nelements;
  
  float fversion = 0;
  fits_get_version(&fversion);

  int bitpix = ULONG_IMG;
  long naxis = 2;
  long naxes[2] = {cam.getpWidth(), cam.getpHeight()};

  if(fits_create_file(&fptr, cam.filename.c_str(), &status))
    printFITSError(status);

  if(fits_create_img(fptr, bitpix, naxis, naxes, &status))
    printFITSError(status);

  fpixel = 1;
  nelements = naxes[0] * naxes[1];
  if(fits_write_img(fptr, TULONG, fpixel, nelements, cam.imgData, &status))
    printFITSError(status);

  // Write date and other information to FITS header
  if(fits_write_date(fptr, &status))
    printFITSError(status);

  if(fits_close_file(fptr, &status))
    printFITSError(status);

  std::cout << "Wrote file: " << cam.filename << std::endl;

  return 0;
}

void printFITSError(int status){
  if(status){
    fits_report_error(stderr, status);
    exit(status);
  }
  return;
}

void printOptionError(std::string option, std::string value){
  std::cerr<< "Invalid value " << value << " for " << option << std::endl;
  exit(1);
}
